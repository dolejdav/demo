package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Entity
@Table(name = "\"Lekar\"")
public class Lekar extends Zamestnanec {
//    @Id
//    @Column(name = "id_zamestnanec", nullable = false)
//    private Integer id;
//
//    @MapsId
//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JoinColumn(name = "id_zamestnanec", nullable = false)
//    private Zamestnanec zamestnanec;

    @Column(name = "telefon", length = 20)
    private String telefon;

    @ManyToMany
    @JoinTable(
            name = "\"LekarOrdinacniDen\"",
            joinColumns = @JoinColumn(name = "id_lekar"),
            inverseJoinColumns = @JoinColumn(name = "id_ordinacni_den"))
    private Set<OrdinacniDen> ordinacniDny;

//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Zamestnanec getZamestnanec() {
//        return zamestnanec;
//    }
//
//    public void setZamestnanec(Zamestnanec zamestnanec) {
//        this.zamestnanec = zamestnanec;
//    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Set<OrdinacniDen> getOrdinacniDny() {
        return ordinacniDny;
    }
}