package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Objects;

@Entity
@Table(name = "\"PrirazeniSmeny\"")
public class PrirazeniSmeny {

    @MapsId("idZdravotniSestra")
    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_zdravotni_sestra", nullable = false)
    private ZdravotniSestra idZdravotniSestra;

    @MapsId("idSmena")
    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_smena", nullable = false)
    private Smena idSmena;

    @ColumnDefault("false")
    @Column(name = "je_vedouci", nullable = false)
    private Boolean jeVedouci = false;

    public ZdravotniSestra getIdZdravotniSestra() {
        return idZdravotniSestra;
    }

    public void setIdZdravotniSestra(ZdravotniSestra idZdravotniSestra) {
        this.idZdravotniSestra = idZdravotniSestra;
    }

    public Smena getIdSmena() {
        return idSmena;
    }

    public void setIdSmena(Smena idSmena) {
        this.idSmena = idSmena;
    }

    public Boolean getJeVedouci() {
        return jeVedouci;
    }

    public void setJeVedouci(Boolean jeVedouci) {
        this.jeVedouci = jeVedouci;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrirazeniSmeny that = (PrirazeniSmeny) o;
        return Objects.equals(idZdravotniSestra, that.idZdravotniSestra) && Objects.equals(idSmena, that.idSmena);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idZdravotniSestra, idSmena);
    }
}