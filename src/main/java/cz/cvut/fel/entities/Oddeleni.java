package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "\"Oddeleni\"")
public class Oddeleni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_oddeleni", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_zodpovedna_osoba", nullable = false)
    private Lekar idZodpovednaOsoba;

    @Column(name = "nazev", nullable = false, length = 64)
    private String nazev;

    @Column(name = "telefon", length = 20)
    private String telefon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Lekar getIdZodpovednaOsoba() {
        return idZodpovednaOsoba;
    }

    public void setIdZodpovednaOsoba(Lekar idZodpovednaOsoba) {
        this.idZodpovednaOsoba = idZodpovednaOsoba;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

}