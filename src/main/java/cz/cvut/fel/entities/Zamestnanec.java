package cz.cvut.fel.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "\"Zamestnanec\"")
@Inheritance(strategy=InheritanceType.JOINED)
public class Zamestnanec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zamestnanec", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nadrizeny")
    private Zamestnanec idNadrizeny;

    @Column(name = "osobni_cislo", nullable = false, length = 20)
    private String osobniCislo;

    @Column(name = "jmeno", nullable = false, length = 64)
    private String jmeno;

    @Column(name = "prijmeni", nullable = false, length = 64)
    private String prijmeni;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Zamestnanec getIdNadrizeny() {
        return idNadrizeny;
    }

    public void setIdNadrizeny(Zamestnanec idNadrizeny) {
        this.idNadrizeny = idNadrizeny;
    }

    public String getOsobniCislo() {
        return osobniCislo;
    }

    public void setOsobniCislo(String osobniCislo) {
        this.osobniCislo = osobniCislo;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

}