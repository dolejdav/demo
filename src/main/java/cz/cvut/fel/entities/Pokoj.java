package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "\"Pokoj\"")
public class Pokoj {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pokoj", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_oddeleni", nullable = false)
    private Oddeleni idOddeleni;

    @Column(name = "cislo", nullable = false)
    private Integer cislo;

    @Column(name = "pocet_luzek", nullable = false)
    private Integer pocetLuzek;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Oddeleni getIdOddeleni() {
        return idOddeleni;
    }

    public void setIdOddeleni(Oddeleni idOddeleni) {
        this.idOddeleni = idOddeleni;
    }

    public Integer getCislo() {
        return cislo;
    }

    public void setCislo(Integer cislo) {
        this.cislo = cislo;
    }

    public Integer getPocetLuzek() {
        return pocetLuzek;
    }

    public void setPocetLuzek(Integer pocetLuzek) {
        this.pocetLuzek = pocetLuzek;
    }

}