package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Entity
@Table(name = "\"ZdravotniSestra\"")
public class ZdravotniSestra extends Zamestnanec {
//    @Id
//    @Column(name = "id_zamestnanec", nullable = false)
//    private Integer id;
//
//    @MapsId
//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JoinColumn(name = "id_zamestnanec", nullable = false)
//    private Zamestnanec zamestnanec;

    @Column(name = "dosazene_vzdelani", length = 64)
    private String dosazeneVzdelani;

    @ManyToMany
    @JoinTable(
            name = "\"PrirazeniSmeny\"",
            joinColumns = @JoinColumn(name = "id_zdravotni_sestra"),
            inverseJoinColumns = @JoinColumn(name = "id_smena"))
    private Set<Smena> prirazeneSmeny;

//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Zamestnanec getZamestnanec() {
//        return zamestnanec;
//    }
//
//    public void setZamestnanec(Zamestnanec zamestnanec) {
//        this.zamestnanec = zamestnanec;
//    }

    public String getDosazeneVzdelani() {
        return dosazeneVzdelani;
    }

    public void setDosazeneVzdelani(String dosazeneVzdelani) {
        this.dosazeneVzdelani = dosazeneVzdelani;
    }

    public Set<Smena> getPrirazeneSmeny() {
        return prirazeneSmeny;
    }
}