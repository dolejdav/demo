package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;

import java.time.LocalDate;

@Entity
@Table(name = "\"Pacient\"")
public class Pacient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pacient", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_pokoj", nullable = false)
    private Pokoj idPokoj;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_osetrujici_lekar", nullable = false)
    private Lekar idOsetrujiciLekar;

    @Column(name = "jmeno", nullable = false, length = 64)
    private String jmeno;

    @Column(name = "prijmeni", nullable = false, length = 64)
    private String prijmeni;

    @Column(name = "datum_narozeni", nullable = false)
    private LocalDate datumNarozeni;

    @Column(name = "cislo_pacienta", nullable = false, length = 20)
    private String cisloPacienta;

    @Column(name = "ulice", length = 64)
    private String ulice;

    @Column(name = "mesto", length = 64)
    private String mesto;

    @Column(name = "smerovaci_cislo", length = 20)
    private String smerovaciCislo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pokoj getIdPokoj() {
        return idPokoj;
    }

    public void setIdPokoj(Pokoj idPokoj) {
        this.idPokoj = idPokoj;
    }

    public Lekar getIdOsetrujiciLekar() {
        return idOsetrujiciLekar;
    }

    public void setIdOsetrujiciLekar(Lekar idOsetrujiciLekar) {
        this.idOsetrujiciLekar = idOsetrujiciLekar;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public LocalDate getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(LocalDate datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public String getCisloPacienta() {
        return cisloPacienta;
    }

    public void setCisloPacienta(String cisloPacienta) {
        this.cisloPacienta = cisloPacienta;
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getSmerovaciCislo() {
        return smerovaciCislo;
    }

    public void setSmerovaciCislo(String smerovaciCislo) {
        this.smerovaciCislo = smerovaciCislo;
    }

}