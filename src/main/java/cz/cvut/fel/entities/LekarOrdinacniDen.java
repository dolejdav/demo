package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Objects;

@Entity
@Table(name = "\"LekarOrdinacniDen\"")
public class LekarOrdinacniDen {

    @MapsId("idOrdinacniDen")
    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_ordinacni_den", nullable = false)
    private OrdinacniDen idOrdinacniDen;

    @MapsId("idLekar")
    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_lekar", nullable = false)
    private Lekar idLekar;

    public OrdinacniDen getIdOrdinacniDen() {
        return idOrdinacniDen;
    }

    public void setIdOrdinacniDen(OrdinacniDen idOrdinacniDen) {
        this.idOrdinacniDen = idOrdinacniDen;
    }

    public Lekar getIdLekar() {
        return idLekar;
    }

    public void setIdLekar(Lekar idLekar) {
        this.idLekar = idLekar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LekarOrdinacniDen that = (LekarOrdinacniDen) o;
        return Objects.equals(idOrdinacniDen, that.idOrdinacniDen) && Objects.equals(idLekar, that.idLekar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOrdinacniDen, idLekar);
    }
}