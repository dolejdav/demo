package cz.cvut.fel.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(name = "\"Smena\"")
public class Smena {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_smena", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "id_oddeleni", nullable = false)
    private Oddeleni idOddeleni;

    @Column(name = "datum", nullable = false)
    private LocalDate datum;

    @Column(name = "cas_od", nullable = false)
    private LocalTime casOd;

    @Column(name = "cas_do", nullable = false)
    private LocalTime casDo;

    @ManyToMany
    @JoinTable(
            name = "\"PrirazeniSmeny\"",
            joinColumns = @JoinColumn(name = "id_smena"),
            inverseJoinColumns = @JoinColumn(name = "id_zdravotni_sestra"))
    private Set<ZdravotniSestra> zdravotniSestry;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Oddeleni getIdOddeleni() {
        return idOddeleni;
    }

    public void setIdOddeleni(Oddeleni idOddeleni) {
        this.idOddeleni = idOddeleni;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public LocalTime getCasOd() {
        return casOd;
    }

    public void setCasOd(LocalTime casOd) {
        this.casOd = casOd;
    }

    public LocalTime getCasDo() {
        return casDo;
    }

    public void setCasDo(LocalTime casDo) {
        this.casDo = casDo;
    }

    public Set<ZdravotniSestra> getZdravotniSestry() {
        return zdravotniSestry;
    }
}