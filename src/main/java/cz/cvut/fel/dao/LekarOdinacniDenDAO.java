package cz.cvut.fel.dao;

import cz.cvut.fel.entities.LekarOrdinacniDen;
import jakarta.persistence.EntityManager;

public class LekarOdinacniDenDAO extends AbstractJpaDAO<LekarOrdinacniDen> {
    public LekarOdinacniDenDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(LekarOrdinacniDen.class);
    }
}
