package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Zamestnanec;
import jakarta.persistence.EntityManager;

public class ZamestnanecDAO extends AbstractJpaDAO<Zamestnanec> {
    public ZamestnanecDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Zamestnanec.class);
    }
}
