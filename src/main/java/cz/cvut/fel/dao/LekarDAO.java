package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Lekar;
import jakarta.persistence.EntityManager;

public class LekarDAO extends AbstractJpaDAO<Lekar>{
    public LekarDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Lekar.class);
    }
}
