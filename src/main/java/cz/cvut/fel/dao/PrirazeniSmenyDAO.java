package cz.cvut.fel.dao;

import cz.cvut.fel.entities.PrirazeniSmeny;
import jakarta.persistence.EntityManager;

public class PrirazeniSmenyDAO extends AbstractJpaDAO<PrirazeniSmeny> {
    public PrirazeniSmenyDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(PrirazeniSmeny.class);
    }
}
