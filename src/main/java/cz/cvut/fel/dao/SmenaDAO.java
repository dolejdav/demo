package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Smena;
import jakarta.persistence.EntityManager;

public class SmenaDAO extends AbstractJpaDAO<Smena>{
    public SmenaDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Smena.class);
    }
}
