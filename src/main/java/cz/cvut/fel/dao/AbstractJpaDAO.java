package cz.cvut.fel.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.List;

public abstract class AbstractJpaDAO< T > {

    private Class< T > clazz;

    @PersistenceContext
    EntityManager entityManager;

    public AbstractJpaDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public final void setClazz( Class< T > clazzToSet ){
        this.clazz = clazzToSet;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public T findOne(long id ){
        return entityManager.find( clazz, id );
    }

    public List< T > findAll(){
        return entityManager.createQuery( "from " + clazz.getName() )
                .getResultList();
    }

    public void create( T entity ){
        entityManager.persist( entity );
    }

    public T update( T entity ){
        return entityManager.merge( entity );
    }

    public void delete( T entity ){
        entityManager.remove( entity );
    }
    public void deleteById( long entityId ){
        T entity = findOne( entityId );
        delete( entity );
    }
}