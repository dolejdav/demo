package cz.cvut.fel.dao;

import cz.cvut.fel.entities.OrdinacniDen;
import jakarta.persistence.EntityManager;

public class OrdinacniDenDAO extends AbstractJpaDAO<OrdinacniDen> {

    public OrdinacniDenDAO(EntityManager em) {
        super(em);
        setClazz(OrdinacniDen.class);
    }
}
