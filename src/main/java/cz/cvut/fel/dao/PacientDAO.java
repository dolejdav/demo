package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Pacient;
import jakarta.persistence.EntityManager;

public class PacientDAO extends AbstractJpaDAO<Pacient>{
    public PacientDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Pacient.class);
    }
}
