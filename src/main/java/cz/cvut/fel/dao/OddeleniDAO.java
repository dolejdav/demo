package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Oddeleni;
import jakarta.persistence.EntityManager;

import java.util.List;

public class OddeleniDAO extends AbstractJpaDAO<Oddeleni>{
    public OddeleniDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Oddeleni.class);
    }

    public Oddeleni findByNazev(String nazev) {
        List<Oddeleni> result = entityManager.createQuery( "from " + getClazz().getName() + " e where e.nazev = :nazev")
                .setParameter("nazev", nazev)
                .getResultList();
        return result.isEmpty() ? null : result.get(0);
    }
}
