package cz.cvut.fel.dao;

import cz.cvut.fel.entities.Oddeleni;
import cz.cvut.fel.entities.Pokoj;
import jakarta.persistence.EntityManager;

import java.util.List;

public class PokojDAO extends AbstractJpaDAO<Pokoj> {
    public PokojDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(Pokoj.class);
    }

    public Pokoj findByOddeleniAndCislo(String nazevOddeleni, int cisloPokoje) {
        List<Pokoj> result = entityManager.createQuery( "from " + getClazz().getName() + " e where e.idOddeleni in (select p.id from " + Oddeleni.class.getName() + " p where p.nazev = :nazevOddeleni) and e.cislo = :cisloPokoje", Pokoj.class )
                .setParameter("cisloPokoje", cisloPokoje)
                .setParameter("nazevOddeleni", nazevOddeleni)
                .getResultList();
        return result.isEmpty() ? null : result.get(0);
    }
}
