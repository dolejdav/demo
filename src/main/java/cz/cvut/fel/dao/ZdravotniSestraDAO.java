package cz.cvut.fel.dao;

import cz.cvut.fel.entities.ZdravotniSestra;
import jakarta.persistence.EntityManager;

public class ZdravotniSestraDAO extends AbstractJpaDAO<ZdravotniSestra> {
    public ZdravotniSestraDAO(EntityManager entityManager) {
        super(entityManager);
        setClazz(ZdravotniSestra.class);
    }
}
