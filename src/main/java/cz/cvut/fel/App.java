package cz.cvut.fel;

import cz.cvut.fel.dao.*;
import cz.cvut.fel.entities.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;


public class App {
    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;

    String nazevOddeleni;

    public void run() {
        init();

        nazevOddeleni = "Oddeleni";
        System.out.println("Oddělení: " + nazevOddeleni);

        System.out.println();

        System.out.println("Vkládání pokoje číslo 11 s 2 lůžky");
        insertPokoj(11, 2);

        System.out.println();

        System.out.println("Pokoje");
        printAllPokoj();

        System.out.println();

        System.out.println("Mazání pokoje číslo 11");
        deletePokoj(11);

        System.out.println();

        System.out.println("Pokoje");
        printAllPokoj();

        System.out.println();

        System.out.println("Změna čísla pokoje pacienta ID 1 na pokoj č. 1");
        updatePacientPokoj(1, 1);

        System.out.println();

        System.out.println("Pacient ID 1");
        printPacient(1);

        System.out.println();

        System.out.println("Změna čísla pokoje pacienta ID 1 na pokoj č. 7");
        updatePacientPokoj(1, 7);

        System.out.println();

        System.out.println("Pacient ID 1");
        printPacient(1);

        System.out.println();

        System.out.println("Lékaři");
        printAllLekar();

        System.out.println();

        System.out.println("Vkládání náhodného lékaře");
        insertRandomLekar();

        System.out.println();

        System.out.println("Lékaři");
        printAllLekar();

        System.out.println();

        System.out.println("Lékař ID 1 s ordinačními dny");
        printLekarWithOrdinacniDny(1);

        System.out.println();

        System.out.println("Směna ID 1 se zdravotními sestrami");
        printSmenaWithZdravotniSestry(1);

        close();
    }

    private void init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("ApplicationPU");
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void close() {
        entityManager.close();
        entityManagerFactory.close();
    }

    private void insertPokoj(int cisloPokoje, int pocetLuzek) {
        EntityTransaction t = entityManager.getTransaction();
        PokojDAO pokojDAO = new PokojDAO(entityManager);

        try {
            t.begin();

            Pokoj pokoj = new Pokoj();
            pokoj.setIdOddeleni(new OddeleniDAO(entityManager).findByNazev(nazevOddeleni));
            pokoj.setPocetLuzek(pocetLuzek);
            pokoj.setCislo(cisloPokoje);
            pokojDAO.create(pokoj);

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void deletePokoj(int cisloPokoje) {
        EntityTransaction t = entityManager.getTransaction();
        PokojDAO pokojDAO = new PokojDAO(entityManager);

        try {
            t.begin();

            Pokoj pokoj = pokojDAO.findByOddeleniAndCislo(nazevOddeleni, cisloPokoje);
            pokojDAO.delete(pokoj);

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void printAllPokoj() {
        EntityTransaction t = entityManager.getTransaction();
        PokojDAO pokojDAO = new PokojDAO(entityManager);

        try {

            List<Pokoj> pokojList = pokojDAO.findAll();
            for (Pokoj pokoj : pokojList) {
                System.out.printf("pokoj číslo %d, počet lůžek: %d%n", pokoj.getCislo(), pokoj.getPocetLuzek());
            }

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void updatePacientPokoj(long id, int cisloPokoje) {
        EntityTransaction t = entityManager.getTransaction();
        PacientDAO pacientDAO = new PacientDAO(entityManager);
        PokojDAO pokojDAO = new PokojDAO(entityManager);

        try {
            t.begin();

            Pacient pacient = pacientDAO.findOne(id);
            pacient.setIdPokoj(pokojDAO.findByOddeleniAndCislo(nazevOddeleni, cisloPokoje));

            pacientDAO.update(pacient);

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void printPacient(long id) {
        EntityTransaction t = entityManager.getTransaction();
        PacientDAO pacientDAO = new PacientDAO(entityManager);

        try {
            t.begin();

            Pacient pacient = pacientDAO.findOne(id);
            System.out.printf("%s %s, číslo: %s, datum narození: %s, číslo pokoje: %d%n", pacient.getJmeno(), pacient.getPrijmeni(), pacient.getCisloPacienta(), pacient.getDatumNarozeni(), pacient.getIdPokoj().getCislo());

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void insertRandomLekar() {
        EntityTransaction t = entityManager.getTransaction();
        LekarDAO lekarDAO = new LekarDAO(entityManager);

        String jmeno = "Random";
        String prijmeni = "Random";
        String cislo = new BigInteger(336,new Random()).toString().substring(0,6);
        String telefon = new BigInteger(336,new Random()).toString().substring(0,9);

        try {
            t.begin();

            Lekar lekar = new Lekar();
            lekar.setJmeno(jmeno);
            lekar.setPrijmeni(prijmeni);
            lekar.setOsobniCislo(cislo);
            lekar.setTelefon(telefon);
            lekarDAO.create(lekar);

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void printAllLekar() {
        EntityTransaction t = entityManager.getTransaction();
        LekarDAO lekarDAO = new LekarDAO(entityManager);

        try {
            t.begin();

            List<Lekar> lekarList = lekarDAO.findAll();
            for (Lekar lekar : lekarList) {
                System.out.printf("%s %s, číslo: %s, telefon: %s%n", lekar.getJmeno(), lekar.getPrijmeni(), lekar.getOsobniCislo(), lekar.getTelefon());
            }

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void printLekarWithOrdinacniDny(long id) {
        EntityTransaction t = entityManager.getTransaction();
        LekarDAO lekarDAO = new LekarDAO(entityManager);

        try {
            t.begin();

            Lekar lekar = lekarDAO.findOne(id);
            System.out.println("Jméno: " + lekar.getJmeno() + " " + lekar.getPrijmeni());
            System.out.println("Ordinační dny: " + String.join(", ", lekar.getOrdinacniDny().stream().map(OrdinacniDen::getDen).toArray(String[]::new)));

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }

    private void printSmenaWithZdravotniSestry(long id) {
        EntityTransaction t = entityManager.getTransaction();
        SmenaDAO smenaDAO = new SmenaDAO(entityManager);

        try {
            t.begin();

            Smena smena = smenaDAO.findOne(id);
            System.out.printf("Datum: %s, %s - %s%n", smena.getDatum(), smena.getCasOd(), smena.getCasDo());
            System.out.println("Zdravotní sestry: " + String.join(", ", smena.getZdravotniSestry().stream().map(e -> e.getJmeno() + " " + e.getPrijmeni()).toArray(String[]::new)));

            t.commit();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            if(t.isActive()) {
                t.rollback();
            }
        }
    }
}
